module github.com/kync/assgutpi1287

go 1.18

require (
	github.com/google/go-cmp v0.5.8
	golang.org/x/sync v0.0.0-20220722155255-886fb9371eb4
	golang.org/x/text v0.3.7
)
