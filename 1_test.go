package tasks

import (
	"strings"
	"testing"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"

	"github.com/google/go-cmp/cmp"
)

type testCase struct {
	raw      string
	expected person
}

// Implement this method
func (t testCase) parse(caser cases.Caser) (p person) {
	// There are two ways to solve this problem, a simple one but inefficient and a complex one but efficient.
	// Complex one is to validate charachters, split names while iterating the characters in the string. No redundant memory allocations, a few nanoseconds gain but diffucult to follow up.
	// Simple one is go with the easy one. More memory allocations, a few nanoseconds loss, but easy to follow up and debug.
	// There is no critisizm about the performance thus I will go with the simple solution which is almost always good to go unless otherwise specified

	p = person{
		FirstName:   "",
		MiddleNames: []string{},
		LastName:    nil,
	}

	parts := strings.Fields(caser.String(t.raw))
	if len(parts) == 0 {
		return
	}

	p.FirstName = parts[0]
	if len(parts) == 1 {
		return
	}

	p.LastName = strPtr(parts[len(parts)-1])
	if len(parts) == 2 {
		return
	}

	p.MiddleNames = parts[1 : len(parts)-1]
	return
}

type person struct {
	FirstName   string
	MiddleNames []string
	LastName    *string
}

func strPtr(v string) *string { return &v }

func TestNameParsing(t *testing.T) {
	swedishCaser := cases.Title(language.Swedish)
	testCases := []testCase{
		{raw: "Michael Daniel Jäger", expected: person{FirstName: "Michael", MiddleNames: []string{"Daniel"}, LastName: strPtr("Jäger")}},
		{raw: "LINUS HARALD christer WAHLGREN", expected: person{FirstName: "Linus", MiddleNames: []string{"Harald", "Christer"}, LastName: strPtr("Wahlgren")}},
		{raw: "Pippilotta Viktualia Rullgardina Krusmynta Efraimsdotter LÅNGSTRUMP", expected: person{FirstName: "Pippilotta", MiddleNames: []string{"Viktualia", "Rullgardina", "Krusmynta", "Efraimsdotter"}, LastName: strPtr("Långstrump")}},
		{raw: "Kalle Anka", expected: person{FirstName: "Kalle", MiddleNames: []string{}, LastName: strPtr("Anka")}},
		{raw: "Kalle  Anka", expected: person{FirstName: "Kalle", MiddleNames: []string{}, LastName: strPtr("Anka")}},
		{raw: "Ghandi", expected: person{FirstName: "Ghandi", MiddleNames: []string{}}},
		{raw: "\t Ghandi \t\r\n", expected: person{FirstName: "Ghandi", MiddleNames: []string{}}},
	}
	for _, test := range testCases {
		t.Run(test.raw, func(t *testing.T) {
			actual := test.parse(swedishCaser)
			if !cmp.Equal(test.expected, actual) {
				t.Log(cmp.Diff(test.expected, actual))
				t.Fail()
			}
		})
	}
}
