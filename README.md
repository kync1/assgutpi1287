# Intro

This test has three parts. In each file there is a table driven test case and it is your task to implement a fix for each one. If you get stuck but you know how to pseudo solve the problem you can write that as a comment. Feel free to refactor code and if you find bugs in the test cases point those out. Place any comments in the code.

## Case 1

Split name strings into their respective parts

## Case 2

Fetch users and populate all of their friends

## Case 3

Validate if two knights in a game of chess can attack each other based on their position.




### Changes in the tests and implementations

##### Test 1

- One of the test case was wrong. fixed: lowered LÅNGSTRUMP to Långstrump
- Added a new test case: multiple whitespaces in the raw name
- Used strings.Fields to split raw into the name parts.


##### Test 2

- It was fun to create a dummy-orm-like db, Apologies to make it complex than it could be. 
- Context(cancellation token) is added to query implementation, 300ms is quite long for a query, needs cancellation. 
- There is no person with ID `350` thus, result should be nil and err ErrPersonNotFound.
-

```go
{
    request: 350,
    result:  nil,
    err:     ErrPersonNotFound,
},
```
- There is a better/correct way to compare two errors: 
```go
     Is(err, target error) bool
```


##### Test 3

- Converted position in letter to rune, makes code cleaner, prevents redundant codes conversions
- Hard coded all potential moves for the knight
- Designed chess board as a two dimensional int array, to make calculations easier/cleaner. 



### Install

- Requires Go 1.8 > or Docker


### Run

#### Run all tests

``sh
go test . 
``

#### Run case 1 tests

``sh
 go test . -run NameParsing
``

#### Or, with docker:
``sh
docker run --rm -it $(docker build -q .) go test -run NameParsing
``


#### Run case 2 tests

``sh
 go test . -run Populate
``

#### Or, with docker:
``sh
docker run --rm -it $(docker build -q .) go test -run Populate
``

#### Run case 2 tests

``sh
 go test . -run Chess
``

#### Or, with docker:
``sh
docker run --rm -it $(docker build -q .) go test -run Chess
``

