package tasks

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"golang.org/x/sync/errgroup"
)

type DBPerson struct {
	ID      int
	Name    string
	Friends []int
}

type PopulatedPerson struct {
	ID      int
	Name    string
	Friends []DBPerson
}

type Database[TId DTIdentifier] struct {
	database map[TId]*DBPerson
}

var (
	ErrNotImplemented = errors.New("not_implemented")
	ErrPersonNotFound = errors.New("account_not_found")
	ErrDBTimeout      = errors.New("db_timeouted")
)

const (
	dbTimeoutInSeconds = 1000
)

func NewDatabase() *Database[int] {
	return &Database[int]{
		database: map[int]*DBPerson{
			621: {ID: 621, Name: "XxDragonSlayerxX", Friends: []int{123, 251, 631}},
			123: {ID: 123, Name: "FriendNo1", Friends: []int{621, 631}},
			251: {ID: 251, Name: "SecondBestFriend", Friends: []int{621}},
			631: {ID: 631, Name: "ThirdWh33l", Friends: []int{621, 123, 251}},
		},
	}
}

func (d *Database[TId]) GetUser(id TId, out chan *DBPerson) error {
	p, ok := d.database[id]
	if !ok {
		return ErrPersonNotFound
	}
	time.Sleep(time.Millisecond * 300)
	// not good idea to return immutable object's pointer unless we don't want to change it from outside.
	pclone := *p
	out <- &pclone
	return nil
}
func (d *Database[TId]) findById(ctx context.Context, id TId) (*DBPerson, error) {
	receiver := make(chan *DBPerson, 1)
	err := d.GetUser(id, receiver)
	if err != nil {
		return nil, err
	}
	select {
	case dbPerson := <-receiver:
		return dbPerson, nil
	case <-ctx.Done():
		return nil, ErrDBTimeout
	}
}

// Implement this method
func populate(db *Database[int], id int) (populatedPerson *PopulatedPerson, err error) {
	ctxTimeout, _ := context.WithTimeout(context.TODO(), time.Second*dbTimeoutInSeconds)
	return populateWithContext(db, id, ctxTimeout)
}

func populateWithContext(db *Database[int], id int, ctx context.Context) (populatedPerson *PopulatedPerson, err error) {
	dbPerson, err := db.findById(ctx, id)
	if err != nil {
		return nil, err
	}
	populatedPerson = &PopulatedPerson{
		ID:      id,
		Name:    dbPerson.Name,
		Friends: nil,
	}
	eagerLoader := DBLoader[int, *DBPerson]{workerCount: 3}
	friends, err := eagerLoader.Load(ctx, db, dbPerson.Friends...)
	if err != nil {
		return populatedPerson, err
	}
	// set friends underlying array size(slice capacity) same as friends to prevent memory allocations in append function.
	populatedPerson.Friends = make([]DBPerson, 0, len(friends))

	// return always in same order
	for _, friendId := range dbPerson.Friends {
		for i := 0; i < len(friends); i++ {
			if friends[i].ID == friendId {
				populatedPerson.Friends = append(populatedPerson.Friends, *friends[i])
			}
		}
	}
	return populatedPerson, nil
}

type DTIdentifier interface {
	string | int
}

type DTObjectQuery[TId DTIdentifier, TObj any] interface {
	findById(context.Context, TId) (TObj, error)
}

type DBLoader[TId DTIdentifier, TObj any] struct {
	workerCount int
}

func (loader *DBLoader[TId, TObj]) Load(ctx context.Context, query DTObjectQuery[TId, TObj], ids ...TId) (loads []TObj, err error) {
	fetchDTObjectJobs := make(chan TId, len(ids))
	results := make(chan TObj, len(ids))
	worker := func(jobs <-chan TId, results chan TObj) error {
		// fail worker if one of the jobs fails with unexpected error
		// otherwise we could create a new struct with dbperson and an error fields and leave error handling to caller.
		for id := range jobs {
			dtobject, err := query.findById(ctx, id)
			if err == ErrPersonNotFound {
				continue
			}
			if err != nil {
				return err
			}
			results <- dtobject
		}
		return nil
	}

	gWorkers, ctx := errgroup.WithContext(ctx)
	gWorkers.SetLimit(loader.workerCount)
	for w := 1; w <= loader.workerCount; w++ {
		gWorkers.Go(func() error {
			return worker(fetchDTObjectJobs, results)
		})
	}

	gResults, ctx := errgroup.WithContext(ctx)
	gResults.SetLimit(1)
	gResults.Go(func() error {
		for dtobject := range results {
			loads = append(loads, dtobject)
		}
		return nil
	})

	for _, id := range ids {
		fetchDTObjectJobs <- id
	}
	close(fetchDTObjectJobs)
	if err = gWorkers.Wait(); err != nil {
		return
	}

	close(results)
	err = gResults.Wait()
	return

}

type testCase2 struct {
	request int
	result  *PopulatedPerson
	err     error
}

func TestPopulate(t *testing.T) {
	testCases := []testCase2{
		{
			request: 621,
			result: &PopulatedPerson{
				ID:   621,
				Name: "XxDragonSlayerxX",
				Friends: []DBPerson{
					{ID: 123, Name: "FriendNo1", Friends: []int{621, 631}},
					{ID: 251, Name: "SecondBestFriend", Friends: []int{621}},
					{ID: 631, Name: "ThirdWh33l", Friends: []int{621, 123, 251}},
				},
			},
			err: nil,
		},
		{
			request: 350,
			result:  nil,
			err:     ErrPersonNotFound,
		},
		{
			request: 251,
			result: &PopulatedPerson{
				ID:   251,
				Name: "SecondBestFriend",
				Friends: []DBPerson{
					{ID: 621, Name: "XxDragonSlayerxX", Friends: []int{123, 251, 631}},
				},
			},
			err: nil,
		},
		{
			request: 631,
			result: &PopulatedPerson{
				ID:   631,
				Name: "ThirdWh33l",
				Friends: []DBPerson{
					{ID: 621, Name: "XxDragonSlayerxX", Friends: []int{123, 251, 631}},
					{ID: 123, Name: "FriendNo1", Friends: []int{621, 631}},
					{ID: 251, Name: "SecondBestFriend", Friends: []int{621}},
				},
			},
			err: nil,
		},
	}

	for ind, test := range testCases {
		t.Run(fmt.Sprint(ind), func(t *testing.T) {
			var db = NewDatabase()
			res, err := populate(db, test.request)
			if !cmp.Equal(res, test.result) {
				t.Log("result is incorrect", cmp.Diff(res, test.result))
				t.Fail()
			}

			if !errors.Is(err, test.err) {
				t.Log("err is incorrect", cmp.Diff(err, test.err))
				t.Fail()
			}
		})
	}
}
