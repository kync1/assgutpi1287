package tasks

import (
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
)

var (
	letterNumberMap = map[rune]int{'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8}
)

type ChessPosition struct {
	Number int
	Letter rune
}
type chessPositionOnBoard struct {
	x int
	y int
}

// Implement me
func canAttack(a, b ChessPosition) bool {
	if !isValidPosition(a) {
		return false
	}
	if !isValidPosition(b) {
		return false
	}

	aPos := chessPositionOnBoard{
		x: letterNumberMap[a.Letter],
		y: a.Number,
	}
	bPos := chessPositionOnBoard{
		x: letterNumberMap[b.Letter],
		y: b.Number,
	}

	knigtMovesX := [8]int{2, 1, -1, -2, -2, -1, 1, 2}
	knigtMovesY := [8]int{1, 2, 2, 1, -1, -2, -2, -1}

	// make all the moves
	for i := 0; i < 8; i++ {
		// calculate the next move position, which may not be valid
		x := aPos.x + knigtMovesX[i]
		y := aPos.y + knigtMovesY[i]
		// is it valid move on the board?
		if x >= 1 && y >= 1 && x < 9 && y < 9 {
			// overlays on the target knight position?
			if x == bPos.x && y == bPos.y {
				return true
			}
		}
	}
	return false

}
func isValidPosition(a ChessPosition) bool {
	if a.Letter < 'A' || a.Letter > 'H' {
		return false
	}
	if a.Number < 1 || a.Number > 8 {
		return false
	}
	return true
}

type testCase3 struct {
	Position1 ChessPosition
	Position2 ChessPosition
	CanAttack bool
}

func TestChess(t *testing.T) {
	testCases := []testCase3{
		{Position1: ChessPosition{Number: 2, Letter: 'C'}, Position2: ChessPosition{Number: 4, Letter: 'D'}, CanAttack: true},
		{Position1: ChessPosition{Number: 7, Letter: 'F'}, Position2: ChessPosition{Number: 5, Letter: 'E'}, CanAttack: true},
		{Position1: ChessPosition{Number: 2, Letter: 'C'}, Position2: ChessPosition{Number: 1, Letter: 'A'}, CanAttack: true},
		{Position1: ChessPosition{Number: 6, Letter: 'A'}, Position2: ChessPosition{Number: 4, Letter: 'B'}, CanAttack: true},
		{Position1: ChessPosition{Number: 6, Letter: 'A'}, Position2: ChessPosition{Number: 5, Letter: 'B'}},
		{Position1: ChessPosition{Number: 2, Letter: 'C'}, Position2: ChessPosition{Number: 2, Letter: 'C'}},
		{Position1: ChessPosition{Number: -1, Letter: 'A'}, Position2: ChessPosition{Number: 1, Letter: 'B'}},
		{Position1: ChessPosition{Number: 4, Letter: 'D'}, Position2: ChessPosition{Number: 5, Letter: 'E'}},
	}
	for ind, test := range testCases {
		t.Run(fmt.Sprint(ind), func(t *testing.T) {
			actual := canAttack(test.Position1, test.Position2)
			if !cmp.Equal(test.CanAttack, actual) {
				t.Log(cmp.Diff(test.CanAttack, actual))
				t.Fail()
			}
		})
	}
}
